package junittests;

public class MovieException extends Exception {

	private String errorMessage;
	
	public MovieException() {
		super();
		errorMessage = "";
	}
	
	public MovieException(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	public String getMessage() {
		return errorMessage;
	}
}
