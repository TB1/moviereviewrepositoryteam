package junittests;

import java.util.List;

import junit.framework.TestCase;
import other.Movie;
import other.MovieDAO;

public class MovieDAOTest extends TestCase {

	protected void setUp() throws Exception {
		super.setUp();
	}
	
	// getAllMovies Test
	// Test No: 001
	// Objective: Test of possibility to get all movies from database
	// Inputs: none
	// Expected Output: not null list with movies
	public void testGetAllMovies001() throws Exception {
		List<Movie> movies = null;
		movies = MovieDAO.getInstance().getAllMovies();
		assertNotNull(movies);
	}
	
	// getAllMovies Test
	// Test No: 002
	// Objective: Test of possibility to get all movies from database
	// Inputs: none
	// Expected Output: null list with no movies
	public void testGetAllMovies002() throws Exception {
		List<Movie> movies = null;
		movies = MovieDAO.getInstance().getAllMovies();
		assertNull(movies);
	}
	
	// addNewMovie Test
	// Test No: 001
	// Objective: Test of possibility to add new movie to database
	// Inputs: title = "Dragon Ball", description = "This movie is about adventures of young sayajin warrion with name Goku.", degree = 7
	// Expected Output: false
	public void testaddNewMovie001() throws Exception {
		String title = "Dragon Ball";
		String description = "This movie is about adventures of young sayajin warrion with name Goku.";
		int degree = 7;
		boolean isAdded = MovieDAO.getInstance().addNewMovie(title, description, degree);
		assertEquals(false, isAdded);
	}
	
	// addNewMovie Test
	// Test No: 002
	// Objective: Test of possibility to add new movie to database
	// Inputs: title = "Dragon Ball Z", description = "This movie is continue of the story of the sayajin Goku.", degree = 8
	// Expected Output: true
	public void testaddNewMovie002() throws Exception {
		String title = "Dragon Ball Z";
		String description = "This movie is continue of the story of the sayajin Goku.";
		int degree = 8;
		boolean isAdded = MovieDAO.getInstance().addNewMovie(title, description, degree);
		assertEquals(true, isAdded);
	}
	
	// addNewMovie Test
	// Test No: 003
	// Objective: Test of possibility to add new movie to database
	// Inputs: title = "Dragon Ball", description = "This movie is about adventures of young sayajin warrion with name Goku.", degree = 7
	// Expected Output: exception
	public void testaddNewMovie003() throws Exception {
		String title = "Dragon Ball";
		String description = "This movie is about adventures of young sayajin warrion with name Goku.";
		int degree = 7;
		try {
			boolean isAdded = MovieDAO.getInstance().addNewMovie(title, description, degree);
		} catch (MovieException e) {
			assertEquals("Movie with this title already exists", e.getMessage());
		}
	}
	
	// searchMovie Test
	// Test No: 001
	// Objective: Test of possibility to search movie in database
	// Inputs: title = "Dragon Ball"
	// Expected Output: not null object
	public void testSearchMovie001() throws Exception {
		String title = "Dragon Ball";
		assertNotNull(MovieDAO.getInstance().searchMovie(title));
	}
	
	// searchMovie Test
	// Test No: 002
	// Objective: Test of possibility to search movie in database
	// Inputs: title = "Matrix"
	// Expected Output: null
	public void testSearchMovie002() throws Exception {
		String title = "Matrix";
		try {
			MovieDAO.getInstance().searchMovie(title);
		} catch (MovieException e) {
			assertEquals("Movie with this title not exists in database", e.getMessage());
		}
	}
	
	// deleteMovie Test
	// Test No: 001
	// Objective: Test of possibility to delete a movie from database
	// Inputs: id = 3
	// Expected Output: true
	public void testDeleteMovie001() throws Exception {
		int id = 3;
		assertEquals(true, MovieDAO.getInstance().deleteMovie(id));
	}
	
	public static void main(String[] args) {
		new MovieDAOTest();
	}
}
