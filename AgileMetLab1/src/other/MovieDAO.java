package other;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import junittests.MovieException;

public class MovieDAO {

	private static MovieDAO movieDAO = null;
	
	private MovieDAO() { }
	
	public static MovieDAO getInstance() {
		if (movieDAO == null) {
			movieDAO = new MovieDAO();
			return movieDAO;
		} else {
			return movieDAO;
		}
	}
	
	public List<Movie> getAllMovies() throws MovieException {
		List<Movie> movies = new ArrayList<>();
		try {
			 ResultSet resultSet = DB.getInstance().executeQuery("SELECT * FROM movie;");
			 while (resultSet.next()) {
				 int id = resultSet.getInt(1);
				 String title = resultSet.getString(2);
				 String description = resultSet.getString(3);
				 int degree = resultSet.getInt(4);
				 Movie movie = new Movie(id, title, description, degree);
				 movies.add(movie);
			 }
		} catch (Exception e) {
			System.out.println(e);
		}
		if (movies.isEmpty()) {
			throw new MovieException("No movies in database");
		}
		return movies;
	}
	
	public boolean addNewMovie(String title, String description, int degree) throws Exception {
		try {
			 ResultSet resultSet = DB.getInstance().executeQuery("SELECT title FROM movie WHERE title = '" + title + "';");
			 if (resultSet.next()) {
				 throw new MovieException("Movie with this title already exists");
				 //return false;
			 } else {
				 DB.getInstance().executeUpdate("INSERT INTO movie(title, description, degree) VALUES('" + title + "', '" + description + "', " + degree + ");");
				 return true;
			 }
		} catch (Exception e) {
			System.out.println(e);
		}
		return false;
	}
	
	public Movie searchMovie(String title) throws MovieException {
		Movie movie = null;
		try {
			 ResultSet resultSet = DB.getInstance().executeQuery("SELECT * FROM movie WHERE title = '" + title + "';");
			 while (resultSet.next()) {
				 int id = resultSet.getInt(1);
				 String description = resultSet.getString(3);
				 int degree = resultSet.getInt(4);
				 movie = new Movie(id, title, description, degree);
			 }
		} catch (Exception e) {
			System.out.println(e);
		}
		if (movie == null) {
			throw new MovieException("Movie with this title not exists in database");
		}
		return movie;
	}
	
	public boolean deleteMovie(int id) throws Exception {
		try {
			 DB.getInstance().executeUpdate("DELETE FROM movie WHERE id = " + id + ";");
			 return true;
		} catch (Exception e) {
			System.out.println(e);
		}
		return false;
	}
}
