package other;

import java.util.List;
import java.util.Scanner;

import junittests.MovieException;
import junittests.UserException;

public class Start {
	
	static boolean loop1 = true, loop2 = true, loop3 = true, loop4 = true, loop5 = true, loop6 = true, loop7 = true;
	static String login, password;

	public static void title() {
		System.out.println();
		System.out.println("---------------------MOVIE REVIEW------------------------------");
		System.out.println();
	}
	
	public static void clearConsole() {
		System.out.println();
		System.out.println("***************************************************");
		System.out.println();
	}
	
	public static void main(String[] args) throws Exception {
		while (loop1) {
			title();
			System.out.println("1. Login");
			System.out.println("2. Register");
			System.out.println("3. Exit");
			System.out.println();
			System.out.print(">> ");
			Scanner scanner = new Scanner(System.in);
			int choose1 = scanner.nextInt();
			clearConsole();
			
			if (choose1 == 1) {
				System.out.print("Login: ");
				scanner = new Scanner(System.in);
				login = scanner.nextLine();
				System.out.print("Password: ");
				scanner = new Scanner(System.in);
				password = scanner.nextLine();
				
				boolean isLogged = false;
				try {
					isLogged = UserDAO.getInstance().login(login, password);
				} catch (UserException e) {
					System.out.println(e);
				}
				
				if (isLogged) {
					while (loop2) {
						title();
						System.out.println("1. Show all movies");
						System.out.println("2. Add new movie");
						System.out.println("3. Search a movie");
						System.out.println("4. Log out");
						if (login.equals("admin") && password.equals("admin")) {
							System.out.println("5. Delete movie");
							System.out.println("6. Delete user");
						}
						System.out.println();
						System.out.print(">> ");
						scanner = new Scanner(System.in);
						int choose2 = scanner.nextInt();
						clearConsole();
						
						if (choose2 == 1) {
							while (loop3) {
								List<Movie> movies = MovieDAO.getInstance().getAllMovies();
								if (movies != null || movies.isEmpty() == false) {
									for (Movie movie : movies) {
										System.out.println("ID: " + movie.getId());
										System.out.println("Title: " + movie.getTitle());
										System.out.println("--------------------------------------------");
									}
									System.out.println();
								} else {
									System.out.println("There is no movies now");
								}
								System.out.println("Choose ID to see details of movie or -1 to back");
								System.out.println();
								System.out.print(">> ");
								scanner = new Scanner(System.in);
								int choose3 = scanner.nextInt();
								clearConsole();
								
								if (choose3 >= 0) {
									while (loop4) {
										for (Movie movie : movies) {
											if (movie.getId() == choose3) {
												System.out.println("Title: " + movie.getTitle());
												System.out.println("Description: " + movie.getDescription());
												System.out.println("Degree: " + movie.getDegree());
											}
										}
										System.out.println();
										System.out.println("1. Back");
										System.out.println();
										System.out.print(">> ");
										scanner = new Scanner(System.in);
										int choose4 = scanner.nextInt();
										
										if (choose4 == 1) {
											break;
										}
									}
								} else {
									break;
								}
							}
						} else if (choose2 == 2) {
							System.out.print("Title: ");
							scanner = new Scanner(System.in);
							String title = scanner.nextLine();
							System.out.print("Description: ");
							scanner = new Scanner(System.in);
							String description = scanner.nextLine();
							System.out.print("Degree: ");
							scanner = new Scanner(System.in);
							int degree = scanner.nextInt();
							boolean isAdded = MovieDAO.getInstance().addNewMovie(title, description, degree);
							if (isAdded) {
								System.out.println("You added a new movie");
							} else {
								System.out.println("A problem with add a new movie");
							}
						} else if (choose2 == 3) {
							System.out.print("Title to search: ");
							scanner = new Scanner(System.in);
							String search = scanner.nextLine();
							try {
								Movie movie = MovieDAO.getInstance().searchMovie(search);
								clearConsole();
								System.out.println("Title: " + movie.getTitle());
								System.out.println("Description: " + movie.getDescription());
								System.out.println("Degree: " + movie.getDegree());
							} catch (MovieException e) {
								System.out.println("Movie with this title not exists in database");
							}
							while (loop5) {
								System.out.println();
								System.out.println("1. Back");
								System.out.println();
								System.out.print(">> ");
								scanner = new Scanner(System.in);
								int choose5 = scanner.nextInt();
								
								if (choose5 == 1) {
									break;
								}
							}
						} else if (choose2 == 4) {
							break;
						}
						if (login.equals("admin") && password.equals("admin") && choose2 == 5) {
							while (loop6) {
								List<Movie> movies = MovieDAO.getInstance().getAllMovies();
								if (movies != null || movies.isEmpty() == false) {
									for (Movie movie : movies) {
										System.out.println("ID: " + movie.getId());
										System.out.println("Title: " + movie.getTitle());
										System.out.println("--------------------------------------------");
									}
									System.out.println();
								} else {
									System.out.println("There is no movies now");
								}
								System.out.println("Choose ID to delete the movie or -1 to back");
								System.out.println();
								System.out.print(">> ");
								scanner = new Scanner(System.in);
								int choose6 = scanner.nextInt();
								clearConsole();
								
								if (choose6 >= 0) {
									int id = choose6;
									MovieDAO.getInstance().deleteMovie(id);
								} else {
									break;
								}
							}
						}
						if (login.equals("admin") && password.equals("admin") && choose2 == 6) {
							while (loop7) {
								List<User> users = UserDAO.getInstance().getAllUsers();
								if (users != null || users.isEmpty() == false) {
									for (User user : users) {
										System.out.println("ID: " + user.getId());
										System.out.println("Login: " + user.getLogin());
										System.out.println("Password: " + user.getPassword());
										System.out.println("--------------------------------------------");
									}
									System.out.println();
								} else {
									System.out.println("There is no users now");
								}
								System.out.println("Choose ID to delete the user or -1 to back");
								System.out.println();
								System.out.print(">> ");
								scanner = new Scanner(System.in);
								int choose7 = scanner.nextInt();
								clearConsole();
								
								if (choose7 >= 0) {
									int id = choose7;
									UserDAO.getInstance().deleteUser(id);
								} else {
									break;
								}
							}
						}
					}
				}
			} else if (choose1 == 2) {
				System.out.print("Login: ");
				scanner = new Scanner(System.in);
				String login = scanner.nextLine();
				System.out.print("Password: ");
				scanner = new Scanner(System.in);
				String password = scanner.nextLine();
				
				boolean isRegistered = UserDAO.getInstance().register(login, password);
				if (isRegistered) {
					System.out.println("Registered succesful");
				} else {
					System.out.println("Register failed");
					clearConsole();
				}
			} else if (choose1 == 3) {
				loop1 = false;
			}
		}
	}
}
