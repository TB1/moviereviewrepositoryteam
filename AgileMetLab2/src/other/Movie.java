package other;

public class Movie {

	private int id;
	private String title;
	private String description;
	private int degree;
	
	public Movie() {
		id = -1;
		title = "";
		description = "";
		degree = -1;
	}
	
	public Movie(int id, String title, String description, int degree) {
		this.id = id;
		this.title = title;
		this.description = description;
		this.degree = degree;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getDegree() {
		return degree;
	}

	public void setDegree(int degree) {
		this.degree = degree;
	}
	
	
}
