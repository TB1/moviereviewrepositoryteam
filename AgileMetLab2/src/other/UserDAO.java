package other;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import junittests.UserException;

public class UserDAO {
	
	private static UserDAO userDAO = null;
	
	private UserDAO() { }
	
	public static UserDAO getInstance() {
		if (userDAO == null) {
			userDAO = new UserDAO();
			return userDAO;
		} else {
			return userDAO;
		}
	}
	
	public boolean register(String login, String password) throws UserException {
		if (login.length() < 4) {
			throw new UserException("Login length is too short");
		}
		if (login.length() > 10) {
			throw new UserException("Login length is too long");
		}
		if (password.length() < 4) {
			throw new UserException("Password length is too short");
		}
		if (password.length() > 10) {
			throw new UserException("Password length is too long");
		}
		try {
			 ResultSet resultSet = DB.getInstance().executeQuery("SELECT login FROM login WHERE login = '" + login + "';");
			 if (resultSet.next()) {
				 return false;
			 } else {
				 DB.getInstance().executeUpdate("INSERT INTO login(login, password) VALUES('" + login + "', '" + password + "');");
				 return true;
			 }
		} catch (Exception e) {
			System.out.println(e);
		}
		return true;
	}

	public boolean login(String login, String password) throws UserException {
		if (login.length() < 4) {
			throw new UserException("Login length is too short");
		}
		if (login.length() > 10) {
			throw new UserException("Login length is too long");
		}
		if (password.length() < 4) {
			throw new UserException("Password length is too short");
		}
		if (password.length() > 10) {
			throw new UserException("Password length is too long");
		}
		try {
			 ResultSet resultSet = DB.getInstance().executeQuery("SELECT login FROM login WHERE login = '" + login + "' AND password = '" + password + "';");
			 if (resultSet.next()) {
				 return true;
			 } else {
				 return false;
			 }
		} catch (Exception e) {
			System.out.println(e);
		}
		return false;
	}
	
	public List<User> getAllUsers() throws UserException {
		List<User> users = new ArrayList<>();
		try {
			 ResultSet resultSet = DB.getInstance().executeQuery("SELECT * FROM login;");
			 while (resultSet.next()) {
				 int id = resultSet.getInt(1);
				 String login = resultSet.getString(2);
				 String password = resultSet.getString(3);
				 User user = new User(id, login, password);
				 users.add(user);
			 }
		} catch (Exception e) {
			System.out.println(e);
		}
		return users;
	}
	
	public boolean deleteUser(int id) throws UserException {
		try {
			 DB.getInstance().executeUpdate("DELETE FROM login WHERE id = " + id + ";");
			 return true;
		} catch (Exception e) {
			System.out.println(e);
		}
		return false;
	}

}
