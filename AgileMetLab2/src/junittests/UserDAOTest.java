package junittests;

import other.User;
import other.UserDAO;

import java.util.List;

import junit.framework.TestCase;

public class UserDAOTest extends TestCase {
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
	}
	
	// Register Test
	// Test No: 001
	// Objective: Test length of login and password
	// Inputs: login = "piotrze", password = "1234"
	// Expected Output: false - if user with this login exists or true - if user not exists
	public void testRegister001() throws Exception {
		String login = "piotrze";
		String password = "1234";
		assertEquals(false, UserDAO.getInstance().register(login, password));
	}

	// Register Test
	// Test No: 002
	// Objective: Test length of login and password
	// Inputs: login = "pio", password = "123"
	// Expected Output: exception
	public void testRegister002() throws Exception {
		String login = "pio";
		String password = "123";
		try {
			UserDAO.getInstance().register(login, password);
			fail("Exception expected");
		} catch (UserException e) {
			//assertSame("Password length is too short", e.getMessage());
			assertSame("Login length is too short", e.getMessage());
		}
	}

	// Register Test
	// Test No: 003
	// Objective: Test length of login and password
	// Inputs: login = "piotrze", password = "123"
	// Expected Output: exception
	public void testRegister003() throws Exception {
		String login = "piotrze";
		String password = "123";
		try {
			UserDAO.getInstance().register(login, password);
			fail("Exception expected");
		} catch (UserException e) {
			assertSame("Password length is too short", e.getMessage());
		}
	}

	// Register Test
	// Test No: 004
	// Objective: Test length of login and password
	// Inputs: login = "pio", password = "1234"
	// Expected Output: exception
	public void testRegister004() throws Exception {
		String login = "pio";
		String password = "1234";
		try {
			UserDAO.getInstance().register(login, password);
			fail("Exception expected");
		} catch (UserException e) {
			assertSame("Login length is too short", e.getMessage());
		}
	}
	
	// Login Test
	// Test No: 001
	// Objective: Test length of login and password
	// Inputs: login = "piotrze", password = "1234"
	// Expected Output: true
	public void testlogin001() throws Exception {
		String login = "piotrze";
		String password = "1234";
		assertEquals(true, UserDAO.getInstance().login(login, password));
	}
	
	// Login Test
	// Test No: 002
	// Objective: Test length of login and password
	// Inputs: login = "pio", password = "123"
	// Expected Output: exception
	public void testLogin002() throws Exception {
		String login = "pio";
		String password = "123";
		try {
			UserDAO.getInstance().login(login, password);
			fail("Exception expected");
		} catch (UserException e) {
			//assertSame("Password length is too short", e.getMessage());
			assertSame("Login length is too short", e.getMessage());
		}
	}

	// Login Test
	// Test No: 003
	// Objective: Test length of login and password
	// Inputs: login = "piotrze", password = "123"
	// Expected Output: exception
	public void testLogin003() throws Exception {
		String login = "piotrze";
		String password = "123";
		try {
			UserDAO.getInstance().login(login, password);
			fail("Exception expected");
		} catch (UserException e) {
			assertSame("Password length is too short", e.getMessage());
		}
	}

	// Login Test
	// Test No: 004
	// Objective: Test length of login and password
	// Inputs: login = "pio", password = "1234"
	// Expected Output: exception
	public void testLogin004() throws Exception {
		String login = "pio";
		String password = "1234";
		try {
			UserDAO.getInstance().login(login, password);
			fail("Exception expected");
		} catch (UserException e) {
			assertSame("Login length is too short", e.getMessage());
		}
	}

	// getAllUsers Test
	// Test No: 001
	// Objective: Test of possibility to get all users from database
	// Inputs: none
	// Expected Output: not null list with users
	public void testGetAllUsers001() throws Exception {
		List<User> users = null;
		users = UserDAO.getInstance().getAllUsers();
		assertNotNull(users);
	}
	
	// getAllUsers Test
	// Test No: 002
	// Objective: Test of possibility to get all users from database
	// Inputs: none
	// Expected Output: null list with no users
	public void testGetAllUsers002() throws Exception {
		List<User> users = null;
		users = UserDAO.getInstance().getAllUsers();
		assertNull(users);
	}
	
	// deleteUser Test
	// Test No: 001
	// Objective: Test of possibility to delete a user from database
	// Inputs: id = 3
	// Expected Output: true
	public void testDeleteUser001() throws Exception {
		int id = 3;
		assertEquals(true, UserDAO.getInstance().deleteUser(id));
	}
	
	public static void main(String[] args) {
		new UserDAOTest();
	}
}
