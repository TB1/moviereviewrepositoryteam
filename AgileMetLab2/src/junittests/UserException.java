package junittests;

public class UserException extends Exception {

	private String errorMessage;
	
	public UserException() {
		super();
		errorMessage = "";
	}
	
	public UserException(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	public String getMessage() {
		return errorMessage;
	}
}
