package other;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;


public class DB {
    
	static DB db = null;
    private Connection connection;
    private Statement statement;
    
    private DB() {
    	initialize();
    }
    
    public static DB getInstance() {
    	if (db == null) {
    		db = new DB();
    		return db;
    	} else {
    		return db;
    	}
    }
    
    private void initialize() {
        try {
        	Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:MovieReview.db");
            statement = connection.createStatement();
        } catch (Exception e) {
            System.err.println(e);
        }
    }
    
    public ResultSet executeQuery(String query) {
        try {
            return statement.executeQuery(query);
        } catch (Exception e) {
            System.err.println(e);
        }
        return null;
    }
    
    public void executeUpdate(String query) {
        try {
            statement.executeUpdate(query);
        } catch (Exception e) {
            System.err.println(e);
        }
    }
}
