package other;
import java.sql.ResultSet;

import junittests.CustomerException;

public class Customer {

	private String login, password;
	
	public Customer() {
		login = "";
		password = "";
	}
	
	public Customer(String login, String password) {
		this.login = login;
		this.password = password;
	}
	
	/*
	 * login - minimum 4 characters, maximum 10 characters
	 * password - minimum 4 characters, maximum 10 characters
	 */
	public boolean register(String login, String password) throws CustomerException {
		if (login.length() < 4) {
			throw new CustomerException("Login length is too short");
		}
		if (login.length() > 10) {
			throw new CustomerException("Login length is too long");
		}
		if (password.length() < 4) {
			throw new CustomerException("Password length is too short");
		}
		if (password.length() > 10) {
			throw new CustomerException("Password length is too long");
		}
		try {
			 ResultSet resultSet = DB.getInstance().executeQuery("SELECT login FROM login WHERE login = '" + login + "';");
			 if (resultSet.next()) {
				 return false;
			 } else {
				 DB.getInstance().executeUpdate("INSERT INTO login(login, password) VALUES('" + login + "', '" + password + "');");
				 return true;
			 }
		} catch (Exception e) {
			System.out.println(e);
		}
		return true;
	}

	public boolean login(String login, String password) throws CustomerException {
		if (login.length() < 4) {
			throw new CustomerException("Login length is too short");
		}
		if (login.length() > 10) {
			throw new CustomerException("Login length is too long");
		}
		if (password.length() < 4) {
			throw new CustomerException("Password length is too short");
		}
		if (password.length() > 10) {
			throw new CustomerException("Password length is too long");
		}
		try {
			 ResultSet resultSet = DB.getInstance().executeQuery("SELECT login FROM login WHERE login = '" + login + "' AND password = '" + password + "';");
			 if (resultSet.next()) {
				 return true;
			 } else {
				 return false;
			 }
		} catch (Exception e) {
			System.out.println(e);
		}
		return false;
	}

}
