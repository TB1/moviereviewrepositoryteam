package junittests;

import other.Customer;
import junit.framework.TestCase;

public class CustomerTest extends TestCase {
	
	Customer customer;
	
	@Override
	protected void setUp() throws Exception {
		customer = new Customer();
	}
	
	// Test No: 001
	// Objective: Test length of login and password
	// Inputs: login = "piotrze", password = "1234"
	// Expected Output: true
	public void testRegister001() throws Exception {
		String login = "piotrze";
		String password = "1234";
		assertEquals(true, customer.register(login, password));
	}
	
	// Test No: 002
	// Objective: Test length of login and password
	// Inputs: login = "pio", password = "123"
	// Expected Output: exception
	public void testRegister002() throws Exception {
		String login = "pio";
		String password = "123";
		try {
			customer.register(login, password);
			fail("Exception expected");
		} catch (CustomerException e) {
			//assertSame("Password length is too short", e.getMessage());
			assertSame("Login length is too short", e.getMessage());
		}
	}
	
	// Test No: 003
	// Objective: Test length of login and password
	// Inputs: login = "piotrze", password = "123"
	// Expected Output: exception
	public void testRegister003() throws Exception {
		String login = "piotrze";
		String password = "123";
		try {
			customer.register(login, password);
			fail("Exception expected");
		} catch (CustomerException e) {
			assertSame("Password length is too short", e.getMessage());
		}
	}
	
	// Test No: 004
	// Objective: Test length of login and password
	// Inputs: login = "pio", password = "1234"
	// Expected Output: exception
	public void testRegister004() throws Exception {
		String login = "pio";
		String password = "1234";
		try {
			customer.register(login, password);
			fail("Exception expected");
		} catch (CustomerException e) {
			assertSame("Login length is too short", e.getMessage());
		}
	}
	
	// Test No: 005
	// Objective: Test length of login and password
	// Inputs: login = "piotrze", password = "1234"
	// Expected Output: true
	public void testLogin001() throws Exception {
		String login = "piotrze";
		String password = "1234";
		assertEquals(true, customer.login(login, password));
	}
	
	// Test No: 006
	// Objective: Test length of login and password
	// Inputs: login = "pio", password = "123"
	// Expected Output: exception
	public void testLogin002() throws Exception {
		String login = "pio";
		String password = "123";
		try {
			customer.login(login, password);
			fail("Exception expected");
		} catch (CustomerException e) {
			//assertSame("Password length is too short", e.getMessage());
			assertSame("Login length is too short", e.getMessage());
		}
	}
	
	// Test No: 007
	// Objective: Test length of login and password
	// Inputs: login = "piotrze", password = "123"
	// Expected Output: exception
	public void testLogin003() throws Exception {
		String login = "piotrze";
		String password = "123";
		try {
			customer.login(login, password);
			fail("Exception expected");
		} catch (CustomerException e) {
			assertSame("Password length is too short", e.getMessage());
		}
	}
	
	// Test No: 008
	// Objective: Test length of login and password
	// Inputs: login = "pio", password = "1234"
	// Expected Output: exception
	public void testRLogin004() throws Exception {
		String login = "pio";
		String password = "1234";
		try {
			customer.login(login, password);
			fail("Exception expected");
		} catch (CustomerException e) {
			assertSame("Login length is too short", e.getMessage());
		}
	}
	
	public static void main(String[] args) {
		new CustomerTest();
	}
}
