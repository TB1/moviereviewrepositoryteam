package junittests;

public class CustomerException extends Exception {

	private String errorMessage;
	
	public CustomerException() {
		super();
		errorMessage = "";
	}
	
	public CustomerException(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	public String getMessage() {
		return errorMessage;
	}
}
